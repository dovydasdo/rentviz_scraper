import math
from urllib.request import urlopen as uReq
from urllib.request import Request
from bs4 import BeautifulSoup as soup
from datetime import date
import time
from random import uniform
import json
n = 1
today = date.today()
out_filename = "D:\\RMtr\\rentViz_scraper\\rent\\" + today.strftime("%m-%d-%y") + "_rent_data.csv"
headers = "id|date|price|city|address|sqm\n"
f = open(out_filename, "w", encoding="utf-8")
f.write(headers)
headers = {
    "User-Agent": 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3'}

data = {}

while True:

    page_url = r"https://www.skelbiu.lt/skelbimai/nekilnojamasis-turtas/nuoma/ilgalaike/butu-nuoma/" + str(n)
    time.sleep(uniform(5, 10))
    req = Request(url=page_url, headers=headers)
    uClient = uReq(req)
    page_soup = soup(uClient.read(), "html.parser")
    uClient.close()

    containers = page_soup.findAll("li", {"class": "simpleAds"})
    num_of_ads = page_soup.find("div", {"id": "adsNumberFilterBar"}).string.split(":")[-1].strip()
    num_of_pages = math.ceil(int(num_of_ads) / 24)
    print(n, " / ", num_of_pages)
    for container in containers:

        try:
            pid = container['id']
        except:
            pid = "null"

        try:
            city = container.find("div", {"class": "adsCity"}).string
        except:
            pid = "null"

        try:
            date = container.find("div", {"class": "adsDate"}).string
        except:
            date = "null"

        try:
            address = container.find("div", {"class": "adsTextReview"}).string.strip()
        except:
            address = "null"

        try:
            price = container.find("div", {"class": "adsPrice"}).span.string
            price = price[:-1].strip().replace(" ", "")
        except:
            price = "null"

        try:
            pfu = container.find("span", {"class": "price-for-unit"}).string
            pfu = pfu[1:-5].strip().replace(" ", "")

        except:
            pfu = "null"

        data[pid] = []

        data[pid].append({
            "date": date,
            "price": price,
            "city": city.replace("|", ","),
            "address": address.replace("|", ","),
            "price-for-unit": pfu
        })

        f.write(pid + "| " + date + "| " + price + "| " + city.replace("|", ",") + "| " + address.replace("|",
                                                                                                          ",") + "| " + pfu + "\n")
    n = n + 1
    if n > num_of_pages:
        break
print(n)
with open("./rent_json/rent-" + today.strftime("%m-%d-%y") + ".json", 'a+') as outfile:
    json.dump(data, outfile, ensure_ascii=False)
f.close()
