import pandas as pd
import glob

path = r'./rent'

all_files = glob.glob(path + "/*.csv")

li = []
for filename in all_files:
    df = pd.read_csv(filename, error_bad_lines=False, delimiter="|")
    df["date"] = filename[7:15]
    df.replace("x", "null")
    df.replace(" x", "null")
    df.replace(" x ", "null")
    df.replace("  x", "null")
    df.replace("  x ", "null")
    li.append(df)
indx = 0
result = pd.concat(li)
result.to_csv("./merged.csv", sep="|")
print(result.describe())
