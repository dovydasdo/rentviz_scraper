import pandas as pd
import numpy as np
import glob
import json

path = r'./rent'

all_files = glob.glob(path + "/*.csv")


def get_sqm(dtf, name):
    sqm = {}
    try:
        dtf = dtf.replace({'x': np.nan})
        dtf = dtf[dtf['sqm '] != "  x "]
        dtf['sqm '] = pd.to_numeric(dtf['sqm '])
        sqm[name] = dtf['sqm '].mean()
    except Exception as e:
        dtf = dtf.replace({' null': np.nan})
        dtf = dtf[dtf['sqm'] != " null"]
        dtf['sqm'] = pd.to_numeric(dtf['sqm'])
        sqm[name] = dtf['sqm'].mean()
    return sqm


def get_price(dtf, name):
    price = {}
    try:
        dtf = dtf.replace({'x': np.nan})
        dtf = dtf[dtf['price'] != "  x "]
        dtf['price'] = dtf['price'].str.replace(r'\D+', '')
        dtf['price'] = pd.to_numeric(dtf['price '])
        price[name] = dtf['price'].mean()
    except Exception as e:
        dtf = dtf.replace({' null': np.nan})
        dtf = dtf[dtf['price'] != " null"]
        dtf['price'] = dtf['price'].str.replace(r'\D+', '')
        dtf['price'] = pd.to_numeric(dtf['price'])
        price[name] = dtf['price'].mean()
    return price


def get_date_price(dtf, name):
    dt = {}
    try:
        dtf = dtf[pd.to_numeric(dtf['price'], errors='coerce').notnull()]
        dtf = dtf[["price", "city"]]
        dtf['price'] = pd.to_numeric(dtf['price'])
        jsn = dtf.groupby(['city']).mean()["price"].to_dict()
        dt[name] = jsn
    except Exception as e:
        print(e)

    return dt


def get_city_price(dtf, name):

    city_names = dtf["city"].unique()
    #print(len(city_names))
    dt = {}
    dtf = dtf[pd.to_numeric(dtf['price'], errors='coerce').notnull()]
    dtf = dtf[["price", "city"]]
    dtf['price'] = pd.to_numeric(dtf['price'])
    for city_name in city_names:
        dt[city_name] = []
        date_dt = []
        tdf = dtf[dtf["city"] == city_name]

        date_dt.append(tdf.groupby(['city']).mean()["price"])

        dt[city_name].append(date_dt)
    return dt


def per_date_to_per_city(vocab):
    dates = vocab.keys()
    names = {}
    for date in dates:
        city_names = vocab[date].keys()
        for name in city_names:
            names[name] = []
    for date in dates:
        city_names = vocab[date].keys()
        for name in city_names:
            date_vocab = {}
            date_vocab[date] = vocab[date][name]
            names[name].append(date_vocab)
    return names

sqm = []
price = []
sqm_pc = []
price_per_date = []

city_price = {}

rez_json = {}
for filename in all_files:
    df = pd.read_csv(filename, error_bad_lines=False, delimiter="|", encoding="utf-8")
    name = filename.split("rent\\")[-1].split(".")[0]
    sqm.append(get_sqm(df, name))
    price.append(get_price(df, name))
    price_per_date.append(get_date_price(df, name))

#print(price_per_date)

new_ppd = {}

for dt in price_per_date:
    for key in dt.keys():
        new_ppd[key] = dt[key]

# for x in new_ppd:
#     print (x)
#     for y in new_ppd[x]:
#         print (y,':',new_ppd[x][y])

print(per_date_to_per_city(new_ppd))


#rez_json["average_sqm"] = sqm
#rez_json["average_price"] = price
#rez_json["price_per_date"] = price_per_date
rez_json["price_per_city"] = per_date_to_per_city(new_ppd)
with open("./stats_city.json", 'w+', encoding="utf-8") as outfile:
    json.dump(rez_json, outfile, ensure_ascii=False)
